// Copyright Epic Games, Inc. All Rights Reserved.

#include "Russes.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Russes, "Russes" );

DEFINE_LOG_CATEGORY(LogRusses)
 