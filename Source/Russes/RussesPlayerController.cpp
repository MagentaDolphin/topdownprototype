// Copyright Epic Games, Inc. All Rights Reserved.

#include "RussesPlayerController.h"
#include "GameFramework/Pawn.h"
#include "RussesCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "IPlayerChar.h"


ARussesPlayerController::ARussesPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	
}

void ARussesPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
	GetWorld()->GetTimerManager().SetTimer(AimingTimerHandle, this, &ARussesPlayerController::CharAim, 0.05f, true);
}

void ARussesPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	FVector AimTarget;
	FHitResult hit;
	
}

void ARussesPlayerController::CharAim()
{
	FVector AimTarget;
	FHitResult hit;
	
	GetHitResultUnderCursor(ECC_WorldStatic, false, hit);
	AimTarget = hit.Location;
	if (IIPlayerChar* CharInterface = Cast<IIPlayerChar>(GetCharacter()))
	{
		CharInterface->Execute_CharAim(GetCharacter(), AimTarget);
	}	
}


void ARussesPlayerController::MoveCharacter(const FInputActionValue& Value)
{
	const FVector2D MoveValue = Value.Get<FVector2D>();
	IIPlayerChar* CharInterface = Cast<IIPlayerChar>(GetCharacter());
	if(CharInterface) CharInterface->Execute_CharMove(GetCharacter(), MoveValue, 1.f);
}

void ARussesPlayerController::CharacterFire(const FInputActionValue& Value)
{

	if (IIPlayerChar* CharInterface = Cast<IIPlayerChar>(GetCharacter()))
	{
		CharInterface->Execute_CharFire(GetCharacter());
	}
	
}



void ARussesPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		EIC->BindAction(SetFireAction, ETriggerEvent::Triggered, this, &ARussesPlayerController::CharacterFire);
		EIC->BindAction(SetMovingActions, ETriggerEvent::Triggered, this, &ARussesPlayerController::MoveCharacter);
		
	}
}
