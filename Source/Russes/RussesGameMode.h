// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RussesGameMode.generated.h"

UCLASS(minimalapi)
class ARussesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARussesGameMode();
};



