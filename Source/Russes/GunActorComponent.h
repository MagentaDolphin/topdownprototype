// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GunStates.h"
#include "PlayerSstates.h"
#include "GunActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class RUSSES_API UGunActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGunActorComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Stats")
	FGunStates States;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Fire")
	bool bCanFire = true;
	UPROPERTY(Replicated, BlueprintReadOnly, VisibleAnywhere, Category = "Aim")
	FVector AimTargetLocation;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Fire")
	UDamageType* DamageType;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Stats")
	FPlayerStates PStates;

protected:

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	FTimerHandle FireTimerHandle;

	bool ResetCanFire();

	FHitResult LineTarceFire(FVector FireSource);
	
	UFUNCTION(Server, Unreliable, WithValidation, BlueprintCallable)
	void Server_Fire(FVector FiringSource);
	bool Server_Fire_Validate(FVector FireSource);
	void Server_Fire_Implementation(FVector FireSource);

	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void Server_UpdateAimTarget(FVector TargetLocation, bool bUseInterpolation);
	void Server_UpdateAimTarget_Implementation(FVector TargetLocation, bool bUseInterpolation);

	UFUNCTION(NetMulticast,Unreliable, BlueprintCallable)
	void Mult_UpdateAimTarget(FVector TargetLocation, bool bUseInterpolation);
	void Mult_UpdateAimTarget_Implementation(FVector TargetLocation, bool bUseInterpolation);


	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_AimGun(USceneComponent* RotationRoot);
	void Server_AimGun_Implementation(USceneComponent* RotationRoot);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void Mult_AimGun(USceneComponent* RotationRoot);
	void Mult_AimGun_Implementation(USceneComponent* RotationRoot);

	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void Server_InitPlayerStats(FPlayerStates PlayerStates);
	void Server_InitPlayerStats_Implementation(FPlayerStates PlayerStates);
	
	virtual void BeginPlay() override;
};
