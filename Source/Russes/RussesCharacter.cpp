// Copyright Epic Games, Inc. All Rights Reserved.

#include "RussesCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "RussesPlayerController.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Math/UnrealMathUtility.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"

ARussesCharacter::ARussesCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 64.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 2000.f;
	CameraBoom->SetRelativeRotation(FRotator(-70.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	
}

void ARussesCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void ARussesCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ARussesCharacter::Destroyed()
{
	Super::Destroyed();
}



void ARussesCharacter::CharMove_Implementation(FVector2D InputVector, float ScaleValue)
{
	if (InputVector.Y != 0)
	{
		AddMovementInput(GetActorForwardVector(), InputVector.Y / InputVector.Length());
	}
	if (InputVector.X != 0)
	{
		AddMovementInput(GetActorRightVector(), InputVector.X / InputVector.Length());
	}
}

void ARussesCharacter::CharFire_Implementation()
{
	IIPlayerChar::CharFire_Implementation();
}

void ARussesCharacter::CharAim_Implementation(FVector Target)
{
	if(HasAuthority())
	{
		Mult_UpdateAimTarget(Target);
		this->CursorPositionInWorld = Target;
	}
	else
	{
		Server_UpdateAimTarget(Target);
	}
	IIPlayerChar::CharAim_Implementation(Target);
}

void ARussesCharacter::GetCharHP_Implementation(int& HCurrentP)
{
	IIPlayerChar::GetCharHP_Implementation(HCurrentP);
}


void ARussesCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ARussesCharacter, CursorPositionInWorld);
	DOREPLIFETIME(ARussesCharacter, HealthPoints);
}


bool ARussesCharacter::Server_UpdateAimTarget_Validate(FVector Target)
{
	return true;
}

void ARussesCharacter::Server_UpdateAimTarget_Implementation(FVector Target)
{
	this->CursorPositionInWorld = Target;
	Mult_UpdateAimTarget(Target);
	UE_LOG(LogTemp, Warning, TEXT("Aim Target Updated On Server"));

}

bool ARussesCharacter::Mult_UpdateAimTarget_Validate(FVector Target)
{
	return true;
}

void ARussesCharacter::Mult_UpdateAimTarget_Implementation(FVector Target)
{
	this->CursorPositionInWorld = Target;
	UE_LOG(LogTemp, Warning, TEXT("Aim Target Updated On Remoute Client"));

}

bool ARussesCharacter::Server_ReleaseDamage_Validate(float Damage)
{
	return true;
}

void ARussesCharacter::Server_ReleaseDamage_Implementation(float Damage)
{
	float DamagetHP = HealthPoints - Damage;
	DamagetHP = FMath::Clamp(DamagetHP, 0.f, MaxHP);
	HealthPoints = DamagetHP;
	if (HealthPoints < 1.f)
	{
		OnDeath();
	}
}

bool ARussesCharacter::OnDeath_Validate()
{
	if (HealthPoints < 1.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ARussesCharacter::OnDeath_Implementation()
{
	this->K2_DestroyActor();
}



