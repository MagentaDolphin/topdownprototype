// Copyright Epic Games, Inc. All Rights Reserved.

#include "RussesGameMode.h"
#include "RussesPlayerController.h"
#include "RussesCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARussesGameMode::ARussesGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARussesPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Player/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Player/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}