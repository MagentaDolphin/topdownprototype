// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IPlayerChar.h"
#include "Net/UnrealNetwork.h"
#include "RussesCharacter.generated.h"

UCLASS(Blueprintable)
class ARussesCharacter : public ACharacter, public IIPlayerChar
{
	GENERATED_BODY()

public:
	ARussesCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	void CharMove_Implementation(FVector2D InputVector, float ScaleValue) override;
	void CharFire_Implementation() override;
	void CharAim_Implementation(FVector Target) override;
	void GetCharHP_Implementation(int& HCurrentP) override;;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

protected:
	
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Aim")
	FVector CursorPositionInWorld;
	UPROPERTY(BlueprintReadOnly, Category = "Aim")
	FVector ActualAimTargetInWorld;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Aim"	)
	float AimHeight = 50.f;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "HP")
	float HealthPoints = 1.f;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "HP")
	float MaxHP = 1.f;
	
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_UpdateAimTarget(FVector Target);
	bool Server_UpdateAimTarget_Validate(FVector Target);
	void Server_UpdateAimTarget_Implementation(FVector Target);
	
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Mult_UpdateAimTarget(FVector Target);
	bool Mult_UpdateAimTarget_Validate(FVector Target);
	void Mult_UpdateAimTarget_Implementation(FVector Target);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void Server_ReleaseDamage(float Damage);
	bool Server_ReleaseDamage_Validate(float Damage);
	void Server_ReleaseDamage_Implementation(float Damage);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void OnDeath();
	bool OnDeath_Validate();
	void OnDeath_Implementation();
};
