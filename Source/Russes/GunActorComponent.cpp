// Fill out your copyright notice in the Description page of Project Settings.


#include "GunActorComponent.h"
#include "GameFramework/GameModeBase.h"
#include "GameModeInterface.h"
#include "GameFramework/GameSession.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UGunActorComponent::UGunActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UGunActorComponent::Server_InitPlayerStats_Implementation(FPlayerStates PlayerStates)
{
	PStates = PlayerStates;
}

// Called when the game starts
void UGunActorComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UGunActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UGunActorComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGunActorComponent, AimTargetLocation);
}

bool UGunActorComponent::ResetCanFire()
{
	bCanFire = true;
	return bCanFire;
}

FHitResult UGunActorComponent::LineTarceFire(FVector FireSource)
{

	FHitResult hit;
	return hit;
}

bool UGunActorComponent::Server_Fire_Validate(FVector FireSource)
{
	if (FireSource != FVector::Zero())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void UGunActorComponent::Server_Fire_Implementation(FVector FireSource)
{

}

void UGunActorComponent::Server_UpdateAimTarget_Implementation(FVector TargetLocation, bool bUseInterpolation)
{
	Mult_UpdateAimTarget(TargetLocation, bUseInterpolation);
	if (bUseInterpolation)
	{
		this->AimTargetLocation = FMath::Lerp(AimTargetLocation, TargetLocation, PStates.Aiming/100.f);
	}
	else
	{
		this->AimTargetLocation = TargetLocation;
	}
	
}

void UGunActorComponent::Mult_UpdateAimTarget_Implementation(FVector TargetLocation, bool bUseInterpolation)
{
	if (bUseInterpolation)
	{
		this->AimTargetLocation = FMath::Lerp(AimTargetLocation, TargetLocation, PStates.Aiming/100.f);
	}
	else
	{
		this->AimTargetLocation = TargetLocation;
	}
	this->AimTargetLocation = TargetLocation;
}

void UGunActorComponent::Server_AimGun_Implementation(USceneComponent* RotationRoot)
{
	FRotator Rotation;
	FVector targetL = AimTargetLocation - RotationRoot->GetComponentLocation();
	targetL.Normalize(0.0001);
	targetL *= States.Range;
	const FVector Start = RotationRoot->GetComponentLocation();
	const FVector End = RotationRoot->GetAttachParentActor()->GetActorLocation() + targetL;
	Rotation = UKismetMathLibrary::FindLookAtRotation(Start, End);
	//Rotation = FMath::Lerp(RotationRoot->GetComponentRotation(), Rotation, 0.3f);

	if (AimTargetLocation != FVector::Zero())
	{
	RotationRoot->SetWorldRotation(Rotation);
	}

	//Mult_AimGun(RotationRoot);
}

void UGunActorComponent::Mult_AimGun_Implementation(USceneComponent* RotationRoot)
{
	FRotator Rotation;
	FVector targetL = AimTargetLocation - RotationRoot->GetComponentLocation();
	targetL.Normalize(0.0001);
	targetL *= States.Range;
	const FVector Start = RotationRoot->GetComponentLocation();
	const FVector End = RotationRoot->GetAttachParentActor()->GetActorLocation() + targetL;
	Rotation = UKismetMathLibrary::FindLookAtRotation(Start, End);
	//Rotation = FMath::Lerp(RotationRoot->GetComponentRotation(), Rotation, 0.3f);

	//if (FMath::Abs((RotationRoot->GetComponentRotation() - Rotation).Yaw) > 7.f)
	//{
		RotationRoot->SetWorldRotation(Rotation);
	//}

}

