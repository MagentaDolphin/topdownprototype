// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameModeInterface.h"
#include "GameFramework/GameModeBase.h"
#include "RussesGameInstance.h"
#include "CompanyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RUSSES_API ACompanyGameModeBase : public AGameModeBase, public IGameModeInterface
{
	GENERATED_BODY()
	

};
