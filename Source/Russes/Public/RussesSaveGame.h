// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PlayerProfile.h"
#include "RussesSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class RUSSES_API URussesSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	FPlayerProfile ProfileStruct; 
};
