﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GameModeInterface.generated.h"


UINTERFACE()
class UGameModeInterface : public UInterface
{
	GENERATED_BODY()
};


class RUSSES_API IGameModeInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Damage")
	void TryToDealPointDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector HitLocation, FVector HitNormal, UPrimitiveComponent* HitComponent, FName BoneName, FVector ShotFromDirection,AController* InstigatedBy, AActor* DamageCauser, const FHitResult& HitInfo);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "GameRules")
	bool GetCanFriendlyFire();
};
