// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerSstates.generated.h"

USTRUCT(BlueprintType)
struct FPlayerStates
{
	GENERATED_BODY();
	
	UPROPERTY(BlueprintReadWrite)
	FName CharName = TEXT("Stoibor");
	UPROPERTY(BlueprintReadWrite)
	float Livability = 100.f;
	UPROPERTY(BlueprintReadWrite)
	float Armor = 10.f;
	UPROPERTY(BlueprintReadWrite)
	float Speed = 600.f;
	UPROPERTY(BlueprintReadWrite)
	float Force = 1.f;
	UPROPERTY(BlueprintReadWrite)
	float Agility = 3.f;
	UPROPERTY(BlueprintReadWrite)
	float Aiming = 40;
	UPROPERTY(BlueprintReadWrite)
	float Mastery = 10.f;
	UPROPERTY(BlueprintReadWrite)
	float Accuracy = 0.f;
	UPROPERTY(BlueprintReadWrite)
	float Felicity = 0.f;
	UPROPERTY(BlueprintReadWrite)
	float Faith = 0.f;
	UPROPERTY(BlueprintReadWrite)
	float Piety = 0.1f;

};
