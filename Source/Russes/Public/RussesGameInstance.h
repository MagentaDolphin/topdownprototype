// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RussesGameInstance.generated.h"


UCLASS(Blueprintable)
class RUSSES_API URussesGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GameRules")
	bool bFriendlyFireEnabled = true;


};
