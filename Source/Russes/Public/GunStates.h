// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GunStates.generated.h"

USTRUCT(BlueprintType)
struct FGunStates
{

	GENERATED_BODY();
	
	UPROPERTY(BlueprintReadWrite)
	FName GunName = TEXT("Base");
    UPROPERTY(BlueprintReadWrite)
    float Lenth = 30.f;
    UPROPERTY(BlueprintReadWrite)
    float Mass = 50.f;
    UPROPERTY(BlueprintReadWrite)
    float BaseDamage = 10.f;
    UPROPERTY(BlueprintReadWrite)
    float CritMult = 1.2f;
    UPROPERTY(BlueprintReadWrite)
    float FireRange = 1.5f;
    UPROPERTY(BlueprintReadWrite)
    float Range = 2000.f;
    UPROPERTY(BlueprintReadWrite)
    float QueueLenth = 1.f;
    UPROPERTY(BlueprintReadWrite)
    UDamageType* DamageType;
 
 
};
