// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerSstates.h"
#include "PlayerProfile.generated.h"

USTRUCT(BlueprintType)
struct FPlayerProfile
{
	GENERATED_BODY();

	UPROPERTY(BlueprintReadWrite)
    FText PlayerName;
    UPROPERTY(BlueprintReadWrite)
    FPlayerStates DefaultStates;
	
};
