// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IPlayerChar.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIPlayerChar : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RUSSES_API IIPlayerChar
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharInterface")
	void CharMove(FVector2D InputVector, float ScaleValue);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharInterface")
	void CharFire();
	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharInterface")
	void CharAim(FVector Target);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharInterface | HP")
	void GetCharHP(int &HCurrentP);
};
